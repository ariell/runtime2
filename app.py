import time

import flask
from flask import render_template
from models import db, migrate
from api import build_api, manager, RaceAPI, unregistered_tags, unregistered_competitors


# Create the Flask application and the Flask-SQLAlchemy object.
from site_modules.flask_restless.views import html_template


def create_app():
    app = flask.Flask(__name__)
    app.config['DEBUG'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/runtime.db'
    app.config["INSTANCE_NAME"] = 'RACE RUNNER v2.0'
    db.init_app(app)
    migrate.init_app(app=app, db=db)
    manager.init_app(app, session=db.session, flask_sqlalchemy_db=db)
    build_api(app)
    return app

app = create_app()

app.jinja_env.filters['format_seconds'] = lambda seconds: time.strftime('%H:%M:%S', time.gmtime(seconds)) if seconds else None


@app.route('/')
def dashboard():
    return render_template("dashboard.html")

@app.route('/api/status')
def status():
    return html_template(dict(
        active_races=RaceAPI.get_active_races(),
        unregistered_tags=unregistered_tags,
        unregistered_competitors=unregistered_competitors
    ))

# start the flask loop
if __name__ == "__main__":
    app.run()