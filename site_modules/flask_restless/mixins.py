from time import time

from flask import request, render_template, jsonify
from savalidation import ValidationMixin, ValidationError
import savalidation.validators as val

class APIModelMixin(ValidationMixin):

    # Key-function pairs containing custom endpoint methods
    api_build_restless_endpoints = True
    api_restless_methods = ['GET']  # , 'POST', 'DELETE', 'PATCH'

    api_custom_endpoints = []  # This defines whether to use a custom method, otherwise if None use restless API model

    # Optional for restless (or custom method)
    api_collection_name = None
    api_url_prefix = '/api'
    api_exclude_columns = None
    api_include_methods = None
    api_include_columns = None
    api_max_results_per_page = 1000
    api_results_per_page = 10
    api_allow_patch_many = False
    api_allow_functions = False  # eg: GET /api/eval/person?q={"functions": [{"name": "count", "field": "id"}]}
    api_validation_exceptions = [ValidationError]
    api_preprocessors = None
    api_postprocessors = None
    api_post_form_preprocessor = None

    # api_primary_key = None

    # Handle validation #######################
    val.validates_constraints()

    def __init__(self):
        pass

    @classmethod
    def register_api(cls, apimanager, app):
        tablename = cls.__tablename__
        apiprefix = apimanager.APINAME_FORMAT.format("/")  # "/api"

        if cls.api_build_restless_endpoints:
            if cls.api_preprocessors is not None:
                cls.api_preprocessors = {k: [getattr(cls, f) for f in cls.api_preprocessors[k]] for k in cls.api_preprocessors.keys()}
            if cls.api_postprocessors is not None:
                cls.api_postprocessors = {k: [getattr(cls, f) for f in cls.api_postprocessors[k]] for k in cls.api_postprocessors.keys()}

            apimanager.create_api(
                cls, methods=cls.api_restless_methods, url_prefix=cls.api_url_prefix,
                collection_name=cls.api_collection_name, allow_patch_many=cls.api_allow_patch_many,
                allow_functions=cls.api_allow_functions, exclude_columns=cls.api_exclude_columns,
                include_columns=cls.api_include_columns, include_methods=cls.api_include_methods,
                validation_exceptions=cls.api_validation_exceptions, results_per_page=cls.api_results_per_page,
                max_results_per_page=cls.api_max_results_per_page, post_form_preprocessor=cls.api_post_form_preprocessor,
                preprocessors=cls.api_preprocessors, postprocessors=cls.api_postprocessors, app=app
            )
            print ("Registered endpoint for: '{}/{}' {}".format(apiprefix, cls.api_collection_name or tablename, cls.api_restless_methods))


        for ep in cls.api_custom_endpoints:
            ep.load_model(cls)
            app.add_url_rule(ep.url(), endpoint=ep.endpoint(), view_func=ep.view_func, methods=ep.methods, defaults=ep.defaults)


class APIModelEndpoint:
    def __init__(self, apiclsmethod, model=None, apiprefix="/api", apiresource="{tablename}", methods=["GET"], defaults=None):
        self.model = None
        self.tablename = None
        self.data_func = None
        self.apiresource = apiresource
        self.apiprefix = apiprefix
        self.methods = methods
        self.apiclassmethod = apiclsmethod
        self.defaults = defaults
        self.load_model(model)

    def url(self, apiprefix=None):
        apiresource = self.apiresource.format(tablename=self.tablename)
        return "{apiprefix}/{apiresource}".format(apiprefix=apiprefix or self.apiprefix, apiresource=apiresource)

    def endpoint(self):
        return f"{self.tablename}.{self.apiclassmethod}_{self.apiresource}"

    def load_model(self, model):
        if model is not None:
            self.model = model
            self.tablename = model.__tablename__
            self.data_func = getattr(model, self.apiclassmethod)
            print(f"Registered endpoint for: '{self.url()}' {self.methods}")

    def view_func(self, **kw):
        kwargs = dict(**request.args, **kw)
        template = kwargs.pop("template", None)
        data = self.data_func(**kwargs)
        if template:
            return render_template(template, args=request.args, data=data, rendered_at=int(time()),
                                   headers=request.headers, base_url=request.base_url, url=request.url)
        else:
            return jsonify(data)
