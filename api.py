# import flask_restless
# from flask import request, render_template, jsonify
import json
import time

from flask import jsonify, request
from sqlalchemy.ext.hybrid import hybrid_property

from site_modules import flask_restless
from models import *
from site_modules.flask_restless.mixins import APIModelEndpoint, APIModelMixin
from site_modules.flask_restless.search import search

manager = flask_restless.APIManager()

unregistered_competitors = {}
unregistered_tags = []


def current_time():
    return datetime.datetime.timestamp()
    # return uptime()   # use uptime (pip install uptime) may be invariant to sys clock changes?


class CompetitorAPI(Competitor, APIModelMixin):
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']


class CompetitorTagAPI(CompetitorTag, APIModelMixin):
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_exclude_columns = ["races"]


class EventAPI(Event, APIModelMixin):
    api_restless_methods = ['POST', 'DELETE', 'PATCH', 'GET']
    api_include_columns = [ "id", "created_at", "name", "estimated_average_nettime", "rules"]


class RaceAPI(Race, APIModelMixin):
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = ["id", "created_at", "event", "event.id", "event.name", "started_at", "ended_at"]

    # api_exclude_columns = ["competitors"]
    api_custom_endpoints = [
        # APIModelEndpoint("get_active", apiresource="{tablename}/active", methods=["GET"]),
        APIModelEndpoint("start_race", apiresource="{tablename}/<int:race_id>/start", methods=["PATCH"]),
        APIModelEndpoint("stop_race", apiresource="{tablename}/<int:race_id>/stop", methods=["PATCH"]),
        APIModelEndpoint("get_state", apiresource="{tablename}/<int:race_id>/state", methods=["GET"]),
        APIModelEndpoint("get_state", apiresource="{tablename}/state", methods=["GET"], defaults={"race_id": None})
    ]

    # @classmethod
    # def get_active(cls):
    #     # query = cls.query.filter(cls.ended_at.is_(None))
    #     objects = [race.to_json() for race in cls.query.filter(cls.ended_at.is_(None)).all()]
    #     return dict(objects=objects)

    @classmethod
    def get_active_races(cls):
        return [dict(id=i[0], event_id=i[1]) for i in db.session.query(Race.id, Race.event_id).filter(Race.is_active==True).all()]

    @classmethod
    def get_state(cls, race_id=None, q=None):
        if race_id is None:
            q = json.loads(q)
            instances = search(db.session, cls, q)
            return dict(objects=[i.to_json() for i in instances.all()], num_results=instances.count(), page=1)
        else:
            return cls.query.get(int(race_id)).to_json()

    @classmethod
    def start_race(cls, race_id=None):
        instance = cls.query.get(int(race_id))
        instance.started_at = datetime.datetime.now()
        db.session.commit()
        return dict(id=race_id, status="started")

    @classmethod
    def stop_race(cls, race_id=None):
        instance = cls.query.get(int(race_id))
        instance.ended_at = datetime.datetime.now()
        db.session.commit()
        return dict(id=race_id, status="ended")

    def to_json(self):
        slowest_expected_nettime = 0
        competitors = []
        for c in self.competitors:
            competitor_race_stats = c.competitor.race_stats(event_id=self.event_id)
            competitors.append(dict(
                first_name=c.competitor.first_name, last_name=c.competitor.last_name, id=c.competitor_id,
                adjustment=c.adjustment, ended_at=c.ended_at, nettime=c.nettime, nettime_normalised=c.nettime_normalised,
                place=c.place, points=c.points, added_time = (datetime.datetime.now()-c.created_at).total_seconds(),
                **competitor_race_stats
            ))
            if slowest_expected_nettime < (competitor_race_stats.get("expected_nettime") or 0):
                slowest_expected_nettime = (competitor_race_stats.get("expected_nettime") or 0)
        # set start time
        for c in competitors:
            c["start_time"] = slowest_expected_nettime - c.get("expected_nettime") + c.get("accumulated_adjustment")
            c["end_time"] = (c.get("ended_at") - self.started_at).total_seconds() if c.get("ended_at", None) else None
        state = "ended" if self.ended_at is not None else "started" if self.started_at is not None else "unstarted"
        curtime = (datetime.datetime.now() - self.started_at).total_seconds() if self.started_at is not None else None
        return dict(
            created_at=self.created_at,
            ended_at=self.ended_at,
            event=dict(name=self.event.name,id=self.event.id,created_at=self.event.created_at),
            id=self.id,
            started_at=self.started_at,
            competitors=competitors,
            slowest_expected_nettime=slowest_expected_nettime,
            # expected_nettime = self.event.expected_nettime(),
            state=state,
            curtime=curtime
        )


class CompetitorRaceAPI(CompetitorRace, APIModelMixin):
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']

    api_custom_endpoints = [
        APIModelEndpoint("set_tag", apiresource="{tablename}/tag/<code>", methods=["GET"]),
    ]

    @classmethod
    def set_tag(cls, code):
        global unregistered_competitors, unregistered_tags
        competitor_race = cls.query.filter(
            cls.competitor_tag.has(CompetitorTag.code == code), cls.race_id==Race.id, Race.is_active==True
        ).one_or_none()
        if competitor_race is not None:
            # Competitor is already in a race and is swiping off (setting end time)
            competitor_race.ended_at = datetime.datetime.now()
            db.session.commit()
            msg="{cr.competitor.first_name} {cr.competitor.last_name} completed race at {cr.ended_at}".format(cr=competitor_race)
        else:
            tag = CompetitorTag.query.filter(CompetitorTag.code == code).one_or_none()
            if tag is not None:
                # tag is found in the system
                race = Race.query.filter(
                    Race.is_active==True, Race.event_id == Event.id, Event.id == CompetitorEvent.event_id,
                    CompetitorEvent.competitor_id==tag.competitor_id)
                if race.count() == 1:
                    # competitor matched to to one active event, so automatically assigned to the active race
                    r = race.one()
                    competitor_race = CompetitorRace(competitor_tag_id=tag.id, competitor_id=tag.competitor_id, race_id=r.id)
                    db.session.add(competitor_race)
                    db.session.commit()
                    msg = "{c.first_name} {c.last_name} added to {r.event.name}".format(c=competitor_race.competitor, r=r)
                else:
                    # competitor is not registered to an active event or is registered to more than one and needs to be assigned
                    c = dict(competitor_tag_id=tag.id, competitor=dict(
                        id=tag.competitor.id, first_name=tag.competitor.first_name, last_name=tag.competitor.last_name))
                    c["msg"] = ("{first_name} {last_name} is not registered to an active event." if race.count() <= 0
                                else "{first_name} {last_name} is registered to more than one active event. You need "
                                     "to assign them to one ONLY. You can update their event registration settings "
                                     "to fix this.").format(**c.get("competitor"))
                    unregistered_competitors[tag.competitor_id] = c
                    return c
            else:
                # new tag? tag_code not registered
                new_tag = dict(code=code, at=datetime.datetime.now(), msg="No tag matching '{}' was found".format(code))
                unregistered_tags.append(new_tag)
                return new_tag
        return dict(competitor_tag_code=code, race_id=competitor_race.race_id, competitor_id=competitor_race.competitor_id, msg=msg)


class CompetitorEventAPI(CompetitorEvent, APIModelMixin):
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']


def build_api(app):
    with app.app_context():
        CompetitorAPI.register_api(manager, app)
        CompetitorTagAPI.register_api(manager, app)
        EventAPI.register_api(manager, app)
        RaceAPI.register_api(manager, app)
        CompetitorRaceAPI.register_api(manager, app)
        CompetitorEventAPI.register_api(manager, app)





