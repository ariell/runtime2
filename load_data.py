import requests

competitors = [
    dict(first_name="Ariell", last_name="Friedman", email="ariell.friedman@gmail.com", tags=[{"id":123456789}]),
    dict(first_name="Kiva", last_name="Galgut", email="kiva@galgut.com.au", tags=[{"id":234567891}]),
    dict(first_name="Nish", last_name="Annear", email="gannear@gadesa.biz", tags=[{"id":345678912}])
]

events = [
    dict(name="Biathlon", rules=dict(rule1="1",rule2="2",rule3=[1,2,3])),
    dict(name="Sunday Swim", rules=dict(rule1="4", rule2="5", rule3=[6, 7, 8]))
]

competitor_events = [
    dict(event_id=1, competitor_id=1),
    dict(event_id=1, competitor_id=2)
]

for c in competitors:
    r = requests.post("http://localhost:5000/api/competitor", json=c)
    print(r.status_code)

for e in events:
    r = requests.post("http://localhost:5000/api/event", json=e)
    print(r.status_code)

for ce in competitor_events:
    r = requests.post("http://localhost:5000/api/competitor_event", json=ce)
    print(r.status_code)