import datetime
import flask_sqlalchemy
from flask_migrate import Migrate


# Create the Flask-Restless API manager.
from sqlalchemy import UniqueConstraint
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref

db = flask_sqlalchemy.SQLAlchemy()
migrate = Migrate()

# Create your Flask-SQLALchemy models as usual but with the following
# restriction: they must have an __init__ method that accepts keyword
# arguments for all columns (the constructor in
# flask_sqlalchemy.SQLAlchemy.Model supplies such a method, so you
# don't need to declare a new one).

default_datetime = datetime.datetime.utcnow


class Competitor(db.Model):
    __tablename__ = "competitor"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, default=default_datetime)
    first_name = db.Column(db.String(120))
    last_name = db.Column(db.String(120))
    email = db.Column(db.String(120), unique=True)

    def race_stats(self, event_id):
        stats = db.session.query(
            # db.func.avg(CompetitorRace.nettime_normalised).label("expected_nettime_normalised"),
            db.func.avg(CompetitorRace.nettime).label("expected_nettime"),
            db.func.sum(CompetitorRace.adjustment).label("accumulated_adjustment")
        ).filter(
            CompetitorRace.competitor_id == self.id,
            CompetitorRace.race.has(Race.event_id == event_id),
            CompetitorRace.nettime.isnot(None)
        ).one()._asdict()
        if stats.get("expected_nettime") is None:
            stats["expected_nettime"] = Event.query.get(event_id).estimated_average_nettime  # db.session.query(Event.estimated_average_nettime).filter(Event.id == event_id).scalar()
        if stats.get("accumulated_adjustment") is None:
            stats["accumulated_adjustment"] = 0
        return stats

class CompetitorTag(db.Model):
    __tablename__ = "competitor_tag"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.Text, unique=True)
    competitor_id = db.Column(db.Integer, db.ForeignKey('competitor.id'))
    competitor = db.relationship(Competitor, backref=db.backref('tags', lazy='dynamic', cascade="all, delete-orphan"), enable_typechecks=False)

class Event(db.Model):
    __tablename__ = "event"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, default=default_datetime)
    name = db.Column(db.String(120))
    estimated_average_nettime = db.Column(db.Float, nullable=False)
    rules = db.Column(db.JSON)

    def expected_nettime(self):
        return db.session.query(db.func.avg(CompetitorRace.nettime)).filter(
            CompetitorRace.nettime.isnot(None), CompetitorRace.race.has(Race.event_id==self.id)).scalar()

class Race(db.Model):
    __tablename__ = "race"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, default=default_datetime)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    event = db.relationship(Event, backref=db.backref('races', lazy='dynamic', cascade="all, delete-orphan"))
    started_at = db.Column(db.DateTime, default=None)
    ended_at = db.Column(db.DateTime, default=None)

    @hybrid_property
    def is_active(self):
        return self.ended_at is None

    @is_active.expression
    def is_active(cls):
        return cls.ended_at.is_(None)


class CompetitorRace(db.Model):
    __tablename__ = "competitor_race"
    # id = Column(Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=default_datetime)
    race_id = db.Column(db.Integer, db.ForeignKey('race.id'), primary_key=True)
    race = db.relationship(Race, backref=backref('competitors', lazy='dynamic'))
    competitor_id = db.Column(db.Integer, db.ForeignKey('competitor.id'), primary_key=True)
    competitor = db.relationship(Competitor, backref=backref('races', lazy='dynamic'))
    competitor_tag_id = db.Column(db.Integer, db.ForeignKey('competitor_tag.id'), nullable=True)
    competitor_tag = db.relationship(CompetitorTag, backref=backref('competitor_races', lazy='dynamic'))
    ended_at = db.Column(db.DateTime)  #, onupdate=default_datetime)
    nettime = db.Column(db.Float)
    adjustment = db.Column(db.Float)
    place = db.Column(db.Integer)
    points = db.Column(db.Integer)
    nettime_normalised = db.Column(db.Float)



class CompetitorEvent(db.Model):
    __tablename__ = "competitor_event"
    # id = Column(Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=default_datetime)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'), primary_key=True)
    competitor_id = db.Column(db.Integer, db.ForeignKey('competitor.id'), primary_key=True)
    nettime_estimate = db.Column(db.Float)
    event = db.relationship(Event, backref=db.backref('competitors', lazy='dynamic', cascade="all, delete-orphan"))
    competitor = db.relationship(Competitor, backref=db.backref('events', lazy='dynamic', cascade="all, delete-orphan"))
